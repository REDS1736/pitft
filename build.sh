#!/bin/sh

python -m build
python -m pip install --force-reinstall dist/pitft-0.0.1-py3-none-any.whl
git add .
git commit -m "chore(build): build package"
git push
