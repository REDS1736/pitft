import time
from PIL import Image

from pitft.display import Display
from pitft.gui_objects import GuiImage, GuiText


def main():
    d = Display(
        canvas_count=2,
    )
    d.turn_backlight_on()
    test_text = GuiText(
        text='Testeditest',
        font_size=30,
        xy=(10, 10),
        color_rgb=(0, 200, 200),
        width=100,
        move_overflow=True,
    )
    d.add_gui_object(
        obj=test_text,
        obj_id='test_text',
        layer=1,
    )
    next_text = GuiText(
        text='Noch ein Test',
        font_size=30,
        xy=(10, 50),
        color_rgb=(200, 0, 200),
        width=200,
    )
    d.add_gui_object(
        obj=next_text,
        obj_id='next_text',
        layer=1
    )
    small_img = GuiImage(
        img=Image.open('examples/cest-la-vie.png'),
        xy=(10, 10),
    )
    d.add_gui_object(
        obj=small_img,
        obj_id='album_img',
        layer=0,
    )

    page2_text = GuiText(
        text='Page 2',
        font_size=30,
        xy=(10, 10),
        color_rgb=(200, 100, 200),
    )
    d.add_gui_object(
        obj=page2_text,
        obj_id='page2_text',
        layer=0,
        canvas_index=1,
    )
    d.update()
    while True:
        try:
            if d.is_button_a_down():
                d.set_active_canvas(0)
            if d.is_button_b_down():
                d.set_active_canvas(1)
            next_text.set_text(str(time.time()))
            d.update()
        except KeyboardInterrupt:
            break
    print('done!')
    d.reset()
    d.turn_backlight_off()


if __name__ == '__main__':
    print('testing.py...')
    main()
