from typing import Optional

from PIL import Image, ImageFont, ImageDraw


class GuiObject:

    def __init__(self):
        pass

    def update(
            self,
            framedelta: float,
    ) -> None:
        pass


class GuiImage(GuiObject):
    _img: Image
    _xy: tuple[int, int]

    def __init__(
            self,
            img: Image,
            xy: tuple[int, int],
    ):
        super().__init__()
        self._img = img
        self._xy = xy

    def get_xy(self):
        return self._xy

    def render_to_image(self) -> Image:
        return self._img

    def update(
            self,
            framedelta: float,
    ) -> None:
        pass


class GuiText(GuiObject):
    _color_rgb: tuple[int, int, int]
    _font: ImageFont
    _font_family: str
    _font_size: int
    _internal_x_offset: int
    _move_overflow: bool
    _move_overflow_padding: int
    _move_overflow_speed: float
    _move_overflow_wait_seconds: float
    __move_overflow_wait_seconds_timer: float
    _text: str
    _xy: tuple[int, int]
    _width: Optional[int]

    def __init__(
            self,
            text: str,
            font_size: int,
            xy: tuple[int, int],
            color_rgb: tuple[int, int, int],
            width=None,
            move_overflow=False,
            move_overflow_padding=20,
            move_overflow_speed=20,
            move_overflow_wait_seconds=2,
            font='DejaVuSansMono.ttf',
    ):
        super().__init__()
        self._color_rgb = color_rgb
        self._font_size = font_size
        self._font_family = font
        self._font = ImageFont.truetype(
            self._font_family,
            self._font_size,
        )
        self._internal_x_offset = 0
        self._move_overflow = move_overflow
        self._move_overflow_padding = move_overflow_padding
        self._move_overflow_speed = move_overflow_speed
        self._move_overflow_wait_seconds = move_overflow_wait_seconds
        self.__move_overflow_wait_seconds_timer = 0
        self._text = text
        self._xy = xy
        self._width = width

    def get_color_rgb(self):
        return self._color_rgb

    def get_font(self):
        return self._font

    def get_text(self):
        return self._text

    def get_xy(self):
        return self._xy

    def set_text(
            self,
            text: str,
    ) -> None:
        self._text = text

    def get_raw_text_size(self) -> tuple[int, int]:
        img = Image.new('RGB', (1000, 1000))
        bb = ImageDraw.ImageDraw(img).textbbox(
            xy=(0, 0),
            text=self._text,
            font=self._font,
            spacing=0,
        )
        text_width = bb[2] - bb[0]
        text_height = bb[3] + bb[1]
        return text_width, text_height

    def render_to_image(self) -> Image:
        text_width, text_height = self.get_raw_text_size()
        box_width = self._width
        if box_width is None:
            box_width = text_width
        img = Image.new(
            'RGBA',
            (box_width, text_height),
            (0, 0, 0, 50),
        )
        img_canvas = ImageDraw.Draw(img)
        img_canvas.text(
            xy=(self._internal_x_offset, 0),
            text=self._text,
            font=self._font,
            fill=self._color_rgb,
        )
        return img

    def update(
            self,
            framedelta: float,
    ) -> None:
        if not (self._move_overflow and self._width is not None):
            return
        if self._internal_x_offset == 0:
            self.__move_overflow_wait_seconds_timer += framedelta
        if self.__move_overflow_wait_seconds_timer > self._move_overflow_wait_seconds:
            self._internal_x_offset -= framedelta * self._move_overflow_speed
            raw_text_width = self.get_raw_text_size()[0]
            if (raw_text_width + self._internal_x_offset) < \
                    (self._width - self._move_overflow_padding):
                self._internal_x_offset = 0
                self.__move_overflow_wait_seconds_timer = 0

