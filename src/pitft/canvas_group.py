from .canvas import Canvas


class CanvasGroup:
    _active_canvas_index: int
    _canvas_list: list[Canvas]

    def __init__(
            self,
            canvas_count=1,
    ):
        self._active_canvas_index = 0
        self._canvas_list = list()
        for c in range(canvas_count):
            self._canvas_list.append(Canvas())

    def get_active_canvas(self) -> Canvas:
        return self._canvas_list[self._active_canvas_index]

    def get_active_canvas_index(self) -> int:
        return self._active_canvas_index

    def get_canvas_count(self) -> int:
        return len(self._canvas_list)

    def set_active_canvas(
            self,
            index: int
    ):
        if index not in range(0, len(self._canvas_list)):
            raise Exception()
        self._active_canvas_index = index
