import board
import time
import digitalio
import displayio
from adafruit_rgb_display import st7789

from .canvas import Canvas
from .canvas_group import CanvasGroup
from .gui_objects import GuiObject


class Display:
    _backlight: digitalio.DigitalInOut
    _button_a: digitalio.DigitalInOut
    _button_b: digitalio.DigitalInOut
    _canvas_group: CanvasGroup
    _display: st7789.ST7789
    _last_update: float
    # list: one item for each canvas in self._canvas_group
    # dict: one key for each layer in this canvas
    # dict: one pair for each id/GuiObject in this layer
    _gui_objects: list[dict[int, dict[str, GuiObject]]]

    def __init__(
            self,
            canvas_count=1,
    ):
        displayio.release_displays()
        self._backlight = digitalio.DigitalInOut(board.D22)
        self._backlight.switch_to_output()
        self._button_a = digitalio.DigitalInOut(board.D23)
        self._button_a.switch_to_input()
        self._button_b = digitalio.DigitalInOut(board.D24)
        self._button_b.switch_to_input()
        self._display = st7789.ST7789(
            board.SPI(),
            cs=digitalio.DigitalInOut(board.CE0),
            dc=digitalio.DigitalInOut(board.D25),
            rst=None,
            baudrate=64_000_000,
            width=135,
            height=240,
            x_offset=53,
            y_offset=40,
            rotation=270,
        )
        self._canvas_group = CanvasGroup(
            canvas_count=canvas_count
        )
        self._last_update = None
        self._gui_objects = list()
        for c in range(canvas_count):
            self._gui_objects.append(dict())
        self.apply_canvas(self._canvas_group.get_active_canvas())

    def get_canvas_group(self) -> CanvasGroup:
        return self._canvas_group

    def apply_canvas(
            self,
            canvas: Canvas
    ) -> None:
        self._display.image(canvas.get_canvas_image())

    def add_gui_object(
            self,
            obj: GuiObject,
            obj_id: str,
            layer: int,
            canvas_index=0,
    ) -> None:
        if canvas_index not in range(self._canvas_group.get_canvas_count()):
            raise Exception()
        this_canvas_objects = self._gui_objects[canvas_index]
        if layer not in this_canvas_objects.keys():
            this_canvas_objects[layer] = dict()
        if obj_id in this_canvas_objects[layer].keys():
            raise Exception()
        this_canvas_objects[layer][obj_id] = obj

    def update(self) -> None:
        self.wipe()
        framedelta = 0.0
        if self._last_update is None:
            self._last_update = time.time()
        else:
            framedelta = time.time() - self._last_update
            self._last_update = time.time()
        this_canvas_objects = self._gui_objects[self._canvas_group.get_active_canvas_index()]
        for layer in sorted(this_canvas_objects.keys()):
            layer_objects = this_canvas_objects[layer]
            for obj_id, obj in layer_objects.items():
                obj.update(framedelta)
                self._canvas_group.get_active_canvas().draw(obj)
        self.apply_canvas(self._canvas_group.get_active_canvas())

    def wipe(self) -> None:
        self._canvas_group.get_active_canvas().fill((255, 255, 255))

    def reset(self) -> None:
        temp_canvas = Canvas()
        temp_canvas.fill((255, 255, 255))
        self.apply_canvas(temp_canvas)

    def set_active_canvas(
            self,
            index: int
    ):
        self.get_canvas_group().set_active_canvas(index)

    def is_button_a_down(self):
        return not self._button_a.value

    def is_button_b_down(self):
        return not self._button_b.value

    def turn_backlight_on(self):
        self._backlight.value = True

    def turn_backlight_off(self):
        self._backlight.value = False
